import boto3
import datetime
import logging
import pprint
import re

logging.basicConfig(level=logging.INFO, format="%(asctime)s-%(levelname)s-%(module)s:%(lineno)s  %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

ec2 = boto3.client('ec2')
route53 = boto3.client('route53')

def getTXTRecords(route53, fqdn, zoneId, filterAutomation=False):
  r"""
  Return an array of TXT records for the given fqdn in the given zoneId.

  Args:
    fqdn            : the fully qualified domain name
    zoneId          : zone id that will house fqdn
    filterAutomation: if True, remove any records like "\s*automation-enabled:\s*true\s*"

  Returns:
    An array of TXT records that look like [ {'Value': ....}, .... ]
  """
  recordSet = route53.list_resource_record_sets(
          HostedZoneId=zoneId,
          StartRecordName=fqdn,
          StartRecordType='TXT')
  if not recordSet:
    return []
  records = list(filter(lambda rs: rs['Name'] == fqdn, recordSet['ResourceRecordSets']))
  if not records:
    return []
  # Do we want to filter out 'automation-enabled:true' records? 
  if not filterAutomation:
    return records[0]['ResourceRecords']

  result = []
  for record in records[0]['ResourceRecords']:
    if not re.match(r'"\s*automation-enabled:\s*true\s*"', record['Value'], re.IGNORECASE):
      result.append(record)
  pprint.pprint(result)
  return result

def checkPermissions(route53, fqdn, zoneId):
  """
  Check the given fqdn to see if we are permitted to updated it in the given zoneId.

  Args:
    fqdn  : the fully qualified domain name
    zoneId: zone id that will house fqdn

  Returns:
    True if we are permitted to update this name, else False    
  """
  recordSet = route53.list_resource_record_sets(
          HostedZoneId=zoneId,
          StartRecordName=fqdn,
          StartRecordType='A')
  if not recordSet:
    logger.debug('No records in this zone. Update permitted.')
    return True

  aRecord = list(filter(lambda rs: rs['Name'] == fqdn, recordSet['ResourceRecordSets']))
  # No A record exists, it's safe to create
  if not aRecord:
    logger.debug('No A record. Update permitted.')
    return True

  txtRecord = getTXTRecords(route53, fqdn, zoneId)
  # When an A record already exists, we have to explicitly allow updates
  if not txtRecord:
    logger.debug('A record exists, but no TXT record. Update denied.')
    return False

  # Check all available TXT records for the string "automation-enabled:true" (space and case insensitive)
  for record in txtRecord:
    if re.match(r'"\s*automation-enabled:\s*true\s*"', record['Value'], re.IGNORECASE):
      logger.debug('TXT record with automation-enabled:true. Update permitted.')
      return True

  # We didn't find a matching TXT record, so deny
  logger.debug('No TXT record with automation:enabled:true. Update denied.')
  return False

def doUpdate(instanceId, fqdn, zoneId, record, recordType, additionalRecords = []):
  """
  Submit a change record of type recordType to Route53 to set the fqdn given in the given zoneId.

  Args:
    instanceId       : the EC2 instance id to which the name will point
    fqdn             : the fully qualified domain name
    zoneId           : zone id that will house fqdn
    record           : the record to be updated (ip address or txt record)
    recordType       : the type of record ('A', 'AAAA', or 'TXT')
    additionalRecords: additional records to include in the update (to preserve existing TXT records)

  Returns:
    Nothing.
  """
  # Based on the zone id, figure out the zone name
  zoneName = route53.get_hosted_zone(Id = zoneId)['HostedZone']['Name']
  resourceRecords = additionalRecords + [ { 'Value': record } ]
  changeRecord =  {
      'Changes': [
        {
          'Action': 'UPSERT',
          'ResourceRecordSet': {
            'Name': fqdn,
            'Type': recordType,
            'TTL': 300,
            'ResourceRecords': resourceRecords
          }
        }
      ]
    }
  logger.info("updating dns for {0} with: {1}".format(instanceId, changeRecord))

  response = route53.change_resource_record_sets(
    HostedZoneId = zoneId,
    ChangeBatch = changeRecord
    )
  logger.debug(response)

def updateRecord(instanceId, name, zoneId, address, recordType = 'A'):
  """
  **If permitted**, submit a change record of type recordType to Route53 to set the fqdn given in the given zoneId.

  Args:
    instanceId: the EC2 instance id to which the name will point
    fqdn      : the fully qualified domain name
    zoneId    : zone id that will house fqdn
    address   : the IP address for the name
    recordType: the type of record ('A' or 'AAAA')

  Returns:
    Nothing.
  """
  zoneName = route53.get_hosted_zone(Id = zoneId)['HostedZone']['Name']
  fqdn = "{0}.{1}".format(name,zoneName)

  # Make sure we aren't stealing a DNS name from someone else
  if not checkPermissions(route53, fqdn, zoneId):
    logger.error("{0} wants to update DNS, but I am not permited to manage {1}".format(instanceId, fqdn))
    return

  doUpdate(instanceId, fqdn, zoneId, address, recordType)
  doUpdate(instanceId, fqdn, zoneId, '"automation-enabled:true"', 'TXT', getTXTRecords(route53, fqdn, zoneId, True))

def handler(event, context):
  """
  Fires when an EC2 instance starts to maintain DNS records for the instance.

  Args:
    event  : data generated by the event
    context: context of the event

  Returns:
    Nothing.
  """
  # Make sure we can get the instance and its tags
  instanceId = event['instance']
  response = ec2.describe_instances(Filters=[{'Name': 'instance-id', 'Values': [instanceId]}])
  instance = response['Reservations'][0]['Instances'][0]
  tags = instance['Tags']

  # Does this instance specify a DNS name for the private IP Address?
  # Normally, probably not, but for instances on a VPN, maybe so.
  privateName = None
  privateZoneId = None
  if [x for x in tags if x['Key'] == 'dns-private-name'] and [x for x in tags if x['Key'] == 'dns-private-zone-id']:
    privateZoneId = [x for x in tags if x['Key'] == 'dns-private-zone-id'][0]['Value']
    privateName = [x for x in tags if x['Key'] == 'dns-private-name'][0]['Value']
    updateRecord(instanceId, privateName, privateZoneId, instance['PrivateIpAddress'])
  else:
    logger.debug('No private name or zone-id specified for instance {0}.'.format(instanceId))


  # Does this instance specify a DNS Name for the public IP Address?
  if [x for x in tags if x['Key'] == 'dns-public-name'] and [x for x in tags if x['Key'] == 'dns-public-zone-id']:
    zoneId = [x for x in tags if x['Key'] == 'dns-public-zone-id'][0]['Value']
    name = [x for x in tags if x['Key'] == 'dns-public-name'][0]['Value']

    # Does the instance actually have a public IP address?
    if 'PublicIpAddress' in instance:
      # Public and private names must differ
      if (name == privateName and zoneId == privateZoneId):
        logger.warning("Instance {0} specifies the same name and zone id for both public and private addresses.".format(instanceId) +
                " Will associate name with public IP address.")

      updateRecord(instanceId, name, zoneId, instance['PublicIpAddress'])
    else:
      logger.warning("Instance {0} has tags 'dns-public-zone-id' and 'dns-public-name', but has no public IP Address.".format(instanceId))

    # If we have an IPv6 address, use the public name for it as well.
    # If we have multiple NIC's or multiple IPv6 addresses, this might not do what is intended.
    if instance['NetworkInterfaces'][0]['Ipv6Addresses']:
      updateRecord(instanceId, name, zoneId, instance['NetworkInterfaces'][0]['Ipv6Addresses'][0], 'AAAA')
  else:
    logger.debug('No public name or zone-id specified for instance {0}.'.format(instanceId))

  logger.info("Done")

if __name__ == "__main__":
  # event template should be
  #testEvent = {
  #  "instance": "$.detail.instance-id",
  #}
  logger.setLevel(logging.DEBUG)
  testEvent = {
    "instance": "i-07a569fe6e4dcbe5e",
  }
  handler(testEvent, None)

