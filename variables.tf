###
# Required
###
variable "responsible_party" {
  description = "Person (pid) who is primarily responsible for the configuration and maintenance of this resource"
}

variable "vcs" {
  description = "A link to the repo in a version control system (usually Git) that manages this resource."
}

###
# Optional
###
variable "compliance_risk" {
  description = "Should be `none`, `ferpa` or `pii`"
  default     = "none"
}

variable "data_risk" {
  description = "Should be `low`, `medium` or `high` based on data-risk classifications defined in VT IT Policies"
  default     = "low"
}

variable "documentation" {
  description = "Link to documentation and/or history file"
  default     = "none"
}

variable "environment" {
  description = "e.g. `development`, `test`, or `production`"
  default     = "production"
}

variable "permitted_zones" {
  description = "List of Route53 zone arns for which this function is permitted to manage DNS"
  default     = ["*"]
  type        = list(string)
}

variable "responsible_party2" {
  description = "Backup for responsible_party"
  default     = "none"
}

variable "service_name" {
  description = "The high level service this resource is primarily supporting"
  default     = "ssh-bastion"
}
