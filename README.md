# aws-ec2-dns-maintainer

## Purpose

Maintain DNS for EC2 instances

## Description

When an EC2 instance starts, fires an event that will trigger a lambda function
to update DNS based on tags in the instance.

![architectural diagram](images/architecture.png)

## Usage Instructions

```
module "test-module" {
  source             = "git::ssh://git@code.vt.edu:devcom/terraform-modules/ec2-dns-maintainer.git"
  environment        = "dvlp"
  permitted_zones    = [ "arn:aws:route53:::hostedzone/ABC123456", "arn:aws:route53:::hostedzone/XYZ654321" ]
  responsible_party  = "sally"
  responsible_party2 = "joe"
  service_name       = "some-service"
  vcs                = "git@code.vt.edu:some/git/repo.git"
}
```

## Preconditions and Assumptions

Assumes you have configured a Route53 Zone ID and will set tags on your EC2 instance as shown below:

To associate a DNS name with the _private IP address_ for an EC2 instance:

| Tag | Description |
| **dns-private-name** | The name (minus domain) that you will use. |
| **dns-private-zone-id** | The Zone **ID** that you want this name to be in |

**Note:** Names on private addresses will probably not be useful unless your instance is running in a VPN.

To associate a DNS name with the _public IP address_ for an EC2 instance:

| Tag | Description |
| **dns-public-name** | The name (minus domain) that you will use. |
| **dns-public-zone-id** | The Zone **ID** that you want this name to be in |

**Note:** You can specify both public and private names for an EC2 instance, but _they must differ_ either in name or zone.

## Inputs

### Required Inputs
These variables are required.

| Name | Type | Description |
| ---- | ---- | ----------- |
| **responsible_party** | `[string]` | person (pid) who is primarily responsible for the configuration and maintenance of this resource |
| **vcs** | `[string]` | A link to the repo in a version control system (usually Git) that manages this resource.

### Optional Inputs
These variables have default values and don't have to be set to use this module.
You may set these variables to override their default values.


| Name | Type | Description | Default |
| ---- | ---- | ----------- | ------- |
| **compliance_risk** | `[string]` | should be `none`, `ferpa` or `pii` | `none`
| **data_risk** | `[string]` | should be `low`, `medium` or `high` based on data-risk classifications defined [here](http://it.vt.edu/content/dam/it_vt_edu/policies/Virginia-Tech-Risk-Classifications.pdf) | `low`
| **documentation** | `[string]` | link to documentation and/or history file | `none`
| **environment** | `[string]` | e.g. `development`, `test`, or `production` | `production`
| **permitted_zones** | `list([string])` | List of Route53 zone arns for which this function is permitted to manage DNS | `["*"]` (all zones)
| **responsible_party2** | `[string]` | Backup for responsible_party | `none`
| **service_name** | `[string]` | The high level service this resource is primarily supporting | `ec2-dns-maintainer`

## Outputs

None.

## Versions

| Version | Major changes |
| ------- | ------------- |
| 1       | Created module |

